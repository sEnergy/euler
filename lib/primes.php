<?php

class MyPrimes {

	/*
	 * Returns array of first $count of prime numbers.
	 */
	static function getPrimes ($count) {
	   
	    $valueLimitFloat = $count * log($count) + $count * (log(log($count - 0.9385)));
	    $valueLimitInt = floor($valueLimitFloat+1);

	    $maxDevider = floor(sqrt($valueLimitInt)+1);

	    $arr = array(2);

	    for ($i = 3; $i < $valueLimitInt; $i+=2) 
	    {
	        $arr[] = $i;
	    }
	    
	    $size = count($arr);
	    $i = 0;

	    // deleting multiples of all numbers
	    while ($arr[$i] <= $maxDevider) 
	    {

	        for ($j = $i+1; $j < $size; $j++) 
	        {

	            if (!isset($arr[$j])) 
	            {    
	                continue;
	            }

	            if (!($arr[$j]%$arr[$i])) 
	            { 
	                unset($arr[$j]);
	            }

	        }

	        while (!isset($arr[++$i]));
	    } 
	    
	    $onlyPrimes = array();
	    
	    for ($i = 0; $i < $size; $i++) 
	    {
	        if (isset($arr[$i])) 
	        {    
	                $onlyPrimes[] = $arr[$i];
	        }
	        
	    }
	    
	    return $onlyPrimes;

	}

	static function isPrime ($x) {

		switch ($x) 
		{
			case 1: 
				return FALSE;

			case 2:
			case 3:
				return TRUE;

			default:
				if (!($x%2) || !($x%3)) 
				{
						return FALSE;
				}

				for ($i = 1; (6*$i)+1 <= $x+6; ++$i) 
				{

					if ((6*$i)+1 == $x || (6*$i)-1 == $x) 
					{
						for ($d = 4; $d < $x; ++$d) 
						{
							if (!($x%$d))
							{
								return FALSE;
							}
						}

						return TRUE;
					}
				}
		}

		return FALSE;
	}

}


