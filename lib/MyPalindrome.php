<?php

class MyPalindrome {

	function isPalindrome ($string)
	{
	    $l = strlen($string);
	    
	    if (!($l%2))
	    {
	        $a = substr($string, 0, ($l/2));
	        $b = strrev(substr($string, ($l/2), $l-1));
	        
	        if ($a==$b)
	            return TRUE;
	        else
	            return FALSE;
	    }
	    else 
	    {
	        $a = substr($string, 0, ($l/2));
	        $b = strrev(substr($string, ($l/2)+1, $l-1));
	        
	        if ($a == $b)
	            return TRUE;
	        else 
	            return FALSE;
	    }
	}

}