<?php

// load source to string
$src = file_get_contents ('sources/large_sum.txt');

// devide to lines
$numbers = explode("\r\n", $src);

//foreach ($lines as $line){
//    echo $line . "\n";
//}

$sum = "";
$rem = 0;

for ($digit = 49; $digit >= 0; --$digit)
{
    $current = $rem;
    $rem = 0;
    
    foreach($numbers as $number)
    {
        $current += $number[$digit]-'0';
    }
    
    $sum = ($current%10) . $sum;
    $rem = floor($current/10);
}

$sum = $rem . $sum;

echo $result = substr($sum, 0, 10);
