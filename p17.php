<?php

$sum = strlen("onethousand");

for($i = 1; $i < 1000; ++$i) 
{
    $h = floor($i/100);
    $r = $i%100;
    
    if ($h)
    {
        $sum += strlen("hundred");
        
        switch($h)
        {
            case 1:
            case 2:
            case 6:
                $sum += 3;
                break;
            case 3:
            case 7:
            case 8: 
                $sum += 5;
                break;
            case 4:
            case 5:
            case 9:
                $sum += 4;
                break;
        }
    }
    
    if ($h && $r)
    {
       $sum += strlen("and"); 
    }
    
    if ($r >= 20)
    {
        $d = floor($r/10);
        $r = $r % 10;
        
        switch($d)
        {
            case 2:
                $sum += strlen("twenty"); 
                break;
            case 3:
                $sum += strlen("thirty"); 
                break;
            case 4:
                $sum += strlen("forty"); 
                break;
            case 5:
                $sum += strlen("fifty"); 
                break;
            case 6:
                $sum += strlen("sixty"); 
                break;
            case 7:
                $sum += strlen("seventy"); 
                break;
            case 8:
                $sum += strlen("eighty"); 
                break;
            case 9:
                $sum += strlen("ninety"); 
                break;
        }
        
        switch($r)
        {
            case 1:
            case 2:
            case 6:
                $sum += 3;
                break;
            case 3:
            case 7:
            case 8: 
                $sum += 5;
                break;
            case 4:
            case 5:
            case 9:
                $sum += 4;
                break;
        }
    }
    else
    {
        switch($r)
        {
            case 1:
            case 2:
            case 6:
            case 10:
                $sum += 3;
                break;
            case 3:
            case 7:
            case 8: 
                $sum += 5;
                break;
            case 4:
            case 5:
            case 9:
                $sum += 4;
                break;
            case 11:
                $sum += strlen("eleven"); 
                break;
            case 12:
                $sum += strlen("twelve"); 
                break;
            case 13:
                $sum += strlen("thirteen"); 
                break;
            case 14:
                $sum += strlen("fourteen"); 
                break;
            case 15:
                $sum += strlen("fifteen"); 
                break;
            case 16:
                $sum += strlen("sixteen"); 
                break;
            case 17:
                $sum += strlen("seventeen"); 
                break;
            case 18:
                $sum += strlen("eighteen"); 
                break;
            case 19:
                $sum += strlen("nineteen"); 
                break;
            
        }
    }  
}

echo $sum;

