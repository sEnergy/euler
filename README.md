# README #

This is repo of my solutions for projecteuler.net problems in PHP.

### What is this repository for? ###

Is serves me as backup, but you can also use it as an inspiration for your solutions.

### Disclaimer ###

There is absolutely no warranty. Furthermore, most of the code is unoptimized, uncomented and often not following any conventions. Everything has been written for the sole purpose of giving me result as fast as possible.

But this will be changed soon :-)