<?php

include 'lib/primes.php';

$primes = getPrimes(150000);
$total = 0;

for ($i = 0; $i < count($primes); $i++) {
        
     if ($primes[$i] < 2000000) {
            $total += $primes[$i];  
     }
     else{
         break;
     }  
}

echo $total;

