<?php

$end = DateTime::createFromFormat('Y-d-m', '2000-31-12');

$date = DateTime::createFromFormat('Y-d-m', '1901-1-1');
$sum = 0;

while ($end != $date){
    
    if ($date->format('l') == 'Sunday' && $date->format('d') == '01')
    {
        $sum++;
    }
    
    $date->modify('+1 day');
    
}

echo $sum;

