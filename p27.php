<?php

require("lib/MyPrimes.php");

$l=0;
$p = 5;

for ($a = -999; $a < 1000; ++$a) {
	echo "$a\n";
	for ($b = -999; $b < 1000; ++$b) {
		
		$i = 0;

		while( MyPrimes::isPrime( ($i*$i) + ($i*$a) + $b ) ) {
			$i++;
		}

		if ($i > $l) {
			$l = $i;
			$p = $a*$b;
		}

	}	
}

echo "product $p";

