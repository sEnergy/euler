<?php

$result = array();

for ($i = 0; $i <= 10000; ++$i){
    $result[$i] = 0;
}

for ($a = 1; $a < 10000; ++$a){
    
    //echo "\n\ntesting $a\n";
    
    $b = 0;
    
    for ($d = 1; $d < $a; ++$d)
    {
        if (!($a%$d))
        {
            $b += $d;
        }
    }
    
    //echo "a sum=$b\n";
    
    
    if ($a == $b)
    {
        continue;
    }
    
    $tester = 0;
    
    for ($d = 1; $d < $b; ++$d)
    {
        if (!($b%$d))
        {
            $tester += $d;
        }
    }
    
   // echo "b sum=$tester\n";
    
    if ($tester == $a)
    {
        $result[$a] = 1;
        $result[$b] = 1;
    }
    
}

$sum = 0;

for ($i = 0; $i <= 10000; ++$i){
    if($result[$i])
    {
        $sum += $i;
    }      
}

echo $sum;