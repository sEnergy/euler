<?php

include "lib/primes.php";

$primes = getPrimes(100000);

$sum = 0;

foreach ($primes as $prime) {

	if ($prime >= 1000000) {
		break;
	}

	if (isCircular($prime)){
		++$sum;
	}

}

echo $sum."\n";


function isCircular ($x) {
	global $primes;

	$number = strval($x);
	$l = strlen($number);

	for ($i = 1; $i < $l; ++$i) {
		$number = $number[strlen($number)-1].substr ($number, 0, strlen($number)-1);

		if (!in_array(intval($number), $primes)) {
			return FALSE;
		}
	}

	return TRUE;
}


