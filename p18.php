<?php

// load source to string
$src = file_get_contents ('sources/p67.txt');

// devide to lines
$lines = explode("\r\n", $src);

// devide to items
$triangleStr = array();

foreach ($lines as $line)
{
    $triangleStr[] = explode(' ', $line);
}


$triangle = array();
$i = 0;
$j = 0;
        
foreach ($triangleStr as $line)
{
    $triangle[$i] = array();
    
    foreach ($line as $item){
        $triangle[$i][$j++] = intval($item);
    }
    
    $j = 0;
    ++$i;
}

unset($triangleStr);

for ($i = 1; $i < count($triangle); ++$i)
{
    $items = count($triangle[$i]);
    
    for ($j = 0; $j < $items; ++$j)
    {
        if ($j == $items-1) // up
        {
            $triangle[$i][$j] += $triangle[$i-1][$j-1];
        }
        else if (!$j) // upleft
        {
            $triangle[$i][$j] += $triangle[$i-1][$j];
        }
        else 
        {
            $a = $triangle[$i-1][$j];
            $b = $triangle[$i-1][$j-1];
            
            $triangle[$i][$j] += ($a > $b)? $a:$b;
        }
    }
}

echo max($triangle[count($triangle)-1]);


