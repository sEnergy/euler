<?php

$a = 1;
$b = 2;

$sum = $b;
$current = 0;

do {
    
    $current = $a + $b;
    
    if ($current > 4000000) {
        break;
    }
    
    if (!($current%2)){
        $sum += $current;
        echo $current."\n";
    }
    
    $a = $b;
    $b = $current;
    
} while (TRUE);

echo $sum;