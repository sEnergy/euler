<?php

$suma = 0;

for ($i = 1; $i < 10000000; ++$i) {

	if (!($i%100000)){
		echo ($i/100000)."%\n";
	}

	$cislo = $i;

	while ($cislo != 1 && $cislo != 89) {

		$n = strval($cislo);
	    $chars = str_split($n);

	    $cislo = 0;

	    foreach ($chars as $c) {
	    	$cislo += intval($c)*intval($c);
	    }  
	}

	if ($cislo == 89) {
		++$suma;
		//echo $i." $cislo\n";
	}
}

echo "Suma je $suma\n";
