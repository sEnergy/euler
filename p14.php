<?php

$winner = 0;
$maxSteps = 0;

$current = 0;
$steps = 0;

for ($i = 2; $i <= 1000000; ++$i){
    
    $steps = 0;
    $current = $i;
    
    while($current != 1){
        
        ++$steps;
        if ($current%2){
            $current = ($current * 3) + 1;
        }
        else {
            $current /= 2;
        }
    }
    
    echo "Number $i - $steps\n";
    
    if ($steps > $maxSteps){
        $maxSteps = $steps;
        $winner = $i;
    }
    
}

echo "Winner is $winner with $maxSteps.";

